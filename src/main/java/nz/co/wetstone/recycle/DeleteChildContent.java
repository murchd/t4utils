package nz.co.wetstone.recycle;

import com.terminalfour.MultiOutputWriter;
import com.terminalfour.database.ConnectionManager;
import com.terminalfour.database.DatabaseException;
import com.terminalfour.hierarchy.HierarchyException;
import com.terminalfour.hierarchy.HierarchyManager;
import com.terminalfour.hierarchy.Section;
import com.terminalfour.utils.T4StreamWriter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DeleteChildContent {

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;

        // Set some defaults
        boolean dryrun = true;
        int parentId = 0;
        String lang = "en";

        // Validate command line args
        if(args.length == 0) {
            System.err.println("No args provided! Usage: \n java -jar recycle.jar parent=12345 dryrun=true lang=en");
            System.exit(1);
        }
        // Process args provided
        for(String arg : args) {
            try {
                String[] kv = arg.split("=");
                if (kv[0].equals("parent")) {
                    parentId = Integer.parseInt(kv[1]);
                } else 
                if (kv[0].equals("dryrun")) {
                    dryrun = Boolean.parseBoolean(kv[1]);
                } else 
                if (kv[0].equals("lang")) {
                    lang = kv[1];
                } else {
		    System.err.println("Invalid param: " + kv[0]);
		}
            } catch(ArrayIndexOutOfBoundsException ignore) {
                System.err.println("Please provide params as key=value");
                System.exit(1);
            } catch(NumberFormatException ignore) {
		System.err.println("Please provide an integer");
		System.exit(1);
	    }
        }

        try {
            // Read the properties file
            Properties props = new Properties();
            InputStream is = new FileInputStream("connection.properties");
            props.load(is);

            //Create connection to DB
            ConnectionManager.getManager().createPool(props.getProperty("driver"), props.getProperty("url"), props.getProperty("username"), props.getProperty("password"), 5, 1);
            MultiOutputWriter outputWriter = new MultiOutputWriter();
            outputWriter.add(new T4StreamWriter(System.out), "SystemOut");
            conn = ConnectionManager.getManager().getConnection();
            stmt = conn.createStatement();

            // Fetch and delete sections
            Section parent = HierarchyManager.getManager().get(stmt, parentId);
            outputWriter.writeln("Parent section: "+parent.getName(lang));
            int[] subSectionIds = HierarchyManager.getManager().getChildren(stmt, parent, lang);
            outputWriter.writeln("Number of sub sections: " + subSectionIds.length);
            for(int sectionId : subSectionIds) {
                Section section = HierarchyManager.getManager().get(stmt, sectionId);
                outputWriter.writeln("Deleting section: "+section.getName(lang));
                if(!dryrun) {
                    HierarchyManager.getManager().delete(stmt, section);
                }
            }
            outputWriter.writeln("Finished");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (DatabaseException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (HierarchyException e) {
            e.printStackTrace();
        } finally {
            if(stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ignore) {}
            }
            try {
                if(conn != null) {
                    ConnectionManager.getManager().returnConnection(conn);
                }
                ConnectionManager.getManager().destroyAllPools();
            } catch (DatabaseException ignore) {
            } catch (SQLException ignore) {}
        }
    }
}
