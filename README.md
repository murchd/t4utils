# Overview

This tool will delete all subsections of the provided parent section id. The deleted sections' content and respective content of child sections will become orphans. Extend the tool to also delete the content first before deleting the sections to remove the seconds step of deleting orphans via the T4 UI.

# Build

`$ mvn clean compile assembly:single`

# Db Config

Edit `connection.properties` to provide db details.

# Run 

`$ java -jar t4-recycle-1.0-SNAPSHOT-jar-with-dependencies.jar parent=281 dryrun=true lang=en`

